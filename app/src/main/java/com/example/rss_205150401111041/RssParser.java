package com.example.rss_205150401111041;

import android.util.Log;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

import java.io.IOException;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

public class RssParser {
    private static String TAG_ITEM = "item";
    private static String TAG_TITLE = "title";
    private static String TAG_CHANNEL = "channel";
    private static String TAG_LINK = "link";


    //mengakses url webnya
    public String loadRssFromUrl(String url) throws IOException {
        Log.d("RSSPARSER", "Start rss parser");
        OkHttpClient client = new OkHttpClient();
        Request request = new Request.Builder()
                .url(url)
                .build();
        Response response = client.newCall(request).execute();
        String xml = response.body().string();
        Log.d("RSSPARSER", "xml " + xml);
        return xml;
    }

    //
    public ArrayList<News> parseRssFromUrl(String xml) throws ParserConfigurationException, IOException, SAXException {
        ArrayList<News> list = new ArrayList<>();
        DocumentBuilderFactory builder = DocumentBuilderFactory.newInstance();
        DocumentBuilder db = builder.newDocumentBuilder();
        InputSource is = new InputSource();
        is.setCharacterStream(new StringReader(xml));
        Document doc = db.parse(is);
        NodeList nodeList = doc.getElementsByTagName(TAG_CHANNEL);
        Element e = (Element) nodeList.item(0);

        NodeList items = e.getElementsByTagName(TAG_ITEM);
        for (int i = 0; i < items.getLength(); i++) {
            News n = new News();
            Element e1 = (Element) items.item(i);

            String judul = getNodeVallue(e1, TAG_TITLE);
            Log.d("parse_judul", judul);
            n.judul=judul;
            list.add(n);

            String link = getNodeVallue(e1, TAG_LINK);
            Log.d("parse_link", link);
            n.tautan=link;

        }
        return list;
    }

    private String getNodeVallue(Element e1, String tagTitle) {
        NodeList  n= e1.getElementsByTagName(tagTitle);
        Node ne = n.item(0);
        Node child = ne.getFirstChild();
        return child.getNodeValue();
    }

}
