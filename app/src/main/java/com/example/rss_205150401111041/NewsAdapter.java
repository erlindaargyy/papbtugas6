package com.example.rss_205150401111041;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class NewsAdapter extends RecyclerView.Adapter<NewsAdapter.NewsViewHolder>{
    LayoutInflater inflater;
    private ArrayList<News> news;
    private Context con;

    public NewsAdapter(Context con, ArrayList<News> news){
        this.con = con;
        this.news = news;
        this.inflater = LayoutInflater.from(this.con);
    }

    public class NewsViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        TextView t;

        public NewsViewHolder(@NonNull View itemView) {
            super(itemView);
            t = itemView.findViewById(R.id.title);
            itemView.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            int position = getAdapterPosition();
            Intent in = new Intent();

            in.addCategory(Intent.CATEGORY_BROWSABLE);
            in.setData(Uri.parse(news.get(position).tautan));
            con.startActivity(in);
        }
    }

    @Override
    public NewsViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = inflater.inflate(R.layout.row, parent, false);
        return new NewsViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull NewsViewHolder holder, int position) {
        News n = news.get(position);
        String judul = n.judul;

        holder.t.setText(judul);
    }

    @Override
    public int getItemCount() {
        return news.size();
    }


}
